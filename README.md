# rvim

VIM with reminis configuration


## Setup RVIM
- Download the latest version of VIM via wget or browser.
- Add the full path to `rvim` to `PATH` environment variable.
- Install that version to `rvim`.
- Clone `Vundle.vim` from GitHub in `rvim/vimfiles/bundle`.
- Run `vim +PluginInstall` and wait for the packages to be downloaded and installed.


## Packages used


## Advanced hotkeys used
